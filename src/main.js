import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import "@fortawesome/fontawesome-free";

createApp(App).use(router).mount("#app");

// TODO: change to real API URL
export const apiUrl = `http://127.0.0.1:3000/`;
